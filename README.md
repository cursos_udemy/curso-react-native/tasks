An react native calendar/scheduler app.

To download the backend application, follow this link: https://gitlab.com/cursos_udemy/curso-react-native/tasks-backend

Front-end
- Make sure that you have an Android or IOS emulator installed. It's possible to use a physical device too;
- Use the node version 8.12.0;
- Execute npm install command at project directory;
- To run the app: react-native run-android (or run-ios).